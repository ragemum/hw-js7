function makeList(arr, parent = document.body) {
    const ul = document.createElement("ul");

    arr.forEach(item => {
        const li = document.createElement("li");

        if(Array.isArray(item)) {
            makeList(item, li);
        } else {
            li.innerHTML = item;
        }

        ul.appendChild(li);  
    });

    parent.appendChild(ul);

    setTimeout(() => {
       ul.remove(); 
    }, 10000);
}


